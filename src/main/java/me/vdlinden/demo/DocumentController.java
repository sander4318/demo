package me.vdlinden.demo;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api")
public class DocumentController {

    @Autowired
    private DocumentService documentService;

    @GetMapping("/documents")
    public List<Document> getAll() {
        return documentService.findAll();
    }

    @GetMapping("/documents/{id}")
    public Document get(@PathVariable Long id) {
        Optional<Document> document = documentService.findById(id);
        if (!document.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Document not found");
        }
        return document.get();
    }

    @PostMapping("/documents")
    public Document post(@Valid @RequestBody Document document) {
        return documentService.save(document);
    }

    @PutMapping("/documents/{id}")
    public Document put(@Valid @RequestBody Document document, @PathVariable Long id) {
        if (!document.getId().equals(id)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Document id and url path id are unequal");
        }
        return documentService.save(document);
    }

    @DeleteMapping("/documents/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        Optional<Document> document = documentService.findById(id);
        if (!document.isPresent()) {
            return ResponseEntity.notFound().build();
            // throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Document not found");
        }
        documentService.delete(documentService.findById(id).get());
        return ResponseEntity.noContent().build();
    }
}
