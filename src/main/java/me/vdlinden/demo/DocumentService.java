package me.vdlinden.demo;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
class DocumentService {

    @Autowired
    private DocumentRepository documentRepository;

    public List<Document> findAll() {
        List<Document> documents = new ArrayList<Document>();
        documentRepository.findAll().forEach(doc -> documents.add(doc));
        return documents;
    }

    public Optional<Document> findById(Long id) {
        return documentRepository.findById(id);
    }

    public Document save(Document document) {
        return documentRepository.save(document);
    }

    public void delete(Document document) {
        documentRepository.delete(document);
    }
}
