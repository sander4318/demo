package me.vdlinden.demo;

import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class Document {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(min = 8, max = 250, message = "Title must be between 8 and 250 characters long")
    private String title;

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.BOOLEAN)
    private Boolean isPublic;

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Timestamp createdAt;

    public Document() {}

    // public Document(Long id, String title, Boolean isPublic, Timestamp createdAt) {
    //     this.id = id;
    //     this.title = title;
    //     this.isPublic = isPublic;
    //     this.createdAt = createdAt;
    // }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(Boolean isPublic) {
        this.isPublic = isPublic;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return String.format("Document{id=%d, title=%s, isPublic=%s, createdAt=%tF}", id, title, isPublic, createdAt);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Document other = (Document) o;
        return id.equals(other.id) &&
                title.equals(other.title) &&
                isPublic.equals(other.isPublic) &&
                createdAt.equals(other.createdAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, isPublic, createdAt);
    }
}
