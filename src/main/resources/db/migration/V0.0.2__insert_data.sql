INSERT INTO document (title, is_public, created_at) VALUES
('test01', true, '2001-01-01 01:01:01'),
('test02', true, '2002-02-02 02:02:02'),
('test03', true, '2003-03-03 03:03:03'),
('test04', false, '2004-04-04 04:04:04'),
('test05', false, '2005-05-05 05:05:05');
